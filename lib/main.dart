import 'package:flutter/material.dart';

//Lab 3 - Chips, buttons and card, EC-Dialog boxes or languages
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Covid-19 Symptom Checker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: UserInfo(),
    );
  }
}

class UserInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //implement bottom navigation
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            showAboutDialog(
                context: context,
                applicationIcon:
                Icon(Icons.ac_unit, size: 60, color: new Color(0xffb2e8ff)),
                applicationName: 'Covid-19 Symptom Checker',
                applicationVersion: '1.0.0',
                applicationLegalese: 'Developed by Tyler, Wizard, Zsuzsanna');
          },
        ),
        title: Text('Covid-19 Symptom Checker'),
        actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        child: Stack(children: [
          Center(
            child: RaisedButton(
              child: Text('Enter Symptoms'),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SymptomScreen()));
              },
            ),
          ),
        ]),
      ),
    );
  }
}

//Tyler
List Symptoms = [];
class SymptomScreen extends StatefulWidget{
  @override
  _SymptomScreenState createState() => _SymptomScreenState();
}

class _SymptomScreenState extends State<SymptomScreen> {
  bool _isSelected = false; // Fevers or chills
  bool _isSelected2 = false; // Coughing
  bool _isSelected3 = false; // Fatigue
  bool _isSelected4 = false; // Difficulty Breathing
  bool _isSelected5 = false; // Body aches
  bool _isSelected6 = false; // Headache
  bool _isSelected7 = false; // Diarrhea
  bool _isSelected8 = false; // Nausea

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffb2e8ff),
      appBar: AppBar(
        backgroundColor: new Color(0xffb2e8ff),
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {},
        ),
        title: Text('Symptoms',
          style: TextStyle(color: Colors.red),
        ),
        actions: <Widget>[],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InputChip(
                    avatar: CircleAvatar(
                      child: Text("Fe"),
                    ),
                    label: Text("Fever or chills"),
                    backgroundColor: new Color(0xffe0f5ff),
                    selected: _isSelected,
                    selectedColor: Colors.redAccent,
                    onSelected: (bool selected){
                      setState(() {
                        _isSelected = selected;
                      });
                    },
                ),
                InputChip(
                    avatar: CircleAvatar(
                      child: Text("Co"),
                    ),
                    label: Text("Coughing"),
                    backgroundColor: new Color(0xffe0f5ff),
                  selected: _isSelected2,
                  selectedColor: Colors.redAccent,
                  onSelected: (bool selected){
                    setState(() {
                      _isSelected2 = selected;
                    });
                  },
                ),
              ],

            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InputChip(
                    avatar: CircleAvatar(
                      child: Text("Fa"),
                    ),
                    label: Text("Fatigue"),
                    backgroundColor: new Color(0xffe0f5ff),
                  selected: _isSelected3,
                  selectedColor: Colors.redAccent,
                  onSelected: (bool selected){
                    setState(() {
                      _isSelected3 = selected;
                    });
                  },
                ),
                InputChip(
                    avatar: CircleAvatar(
                      child: Text("Br"),
                    ),
                    label: Text("Difficulty Breathing"),
                    backgroundColor: new Color(0xffe0f5ff),
                  selected: _isSelected4,
                  selectedColor: Colors.redAccent,
                  onSelected: (bool selected){
                    setState(() {
                      _isSelected4 = selected;
                    });
                  },
                ),

              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InputChip(
                    avatar: CircleAvatar(
                      child: Text("Ac"),
                    ),
                    label: Text("Body aches"),
                    backgroundColor: new Color(0xffe0f5ff),
                    selected: _isSelected5,
                    selectedColor: Colors.redAccent,
                    onSelected: (bool selected){
                    setState(() {
                      _isSelected5 = selected;
                    });
                  },
                ),
                InputChip(
                    avatar: CircleAvatar(
                      child: Text("He"),
                    ),
                    label: Text("Headache"),
                    backgroundColor: new Color(0xffe0f5ff),
                  selected: _isSelected6,
                  selectedColor: Colors.redAccent,
                  onSelected: (bool selected){
                    setState(() {
                      _isSelected6 = selected;
                    });
                  },
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InputChip(
                    avatar: CircleAvatar(
                      child: Text("Di"),
                    ),
                    label: Text("Diarrhea"),
                    backgroundColor: new Color(0xffe0f5ff),
                    selected: _isSelected7,
                    selectedColor: Colors.redAccent,
                    onSelected: (bool selected){
                      setState(() {
                        _isSelected7 = selected;
                    });
                  },
                ),
                InputChip(
                    avatar: CircleAvatar(
                      child: Text("Na"),
                    ),
                    label: Text("Nausea"),
                    backgroundColor: new Color(0xffe0f5ff),
                    selected: _isSelected8,
                    selectedColor: Colors.redAccent,
                    onSelected: (bool selected){
                      setState(() {
                        _isSelected8 = selected;
                    });
                  },
                ),
              ],

            ),


            Align(
              alignment: Alignment.bottomCenter,
                child: RaisedButton(
                  color: new Color(0xffe0f5ff),
                  child: Text('Check Results'),
                  onPressed: () {
                    Symptoms.clear();
                    if(_isSelected == true)
                      {
                        Symptoms.add("Fevers or chills");
                      }
                    if(_isSelected2 == true)
                    {
                      Symptoms.add("Coughing");
                    }

                    if(_isSelected3 == true)
                    {
                      Symptoms.add("Fatigue");
                    }

                    if(_isSelected4 == true)
                    {
                      Symptoms.add("Difficulty Breathing");
                    }

                    if(_isSelected5 == true)
                    {
                      Symptoms.add("Body aches");
                    }

                    if(_isSelected6 == true)
                    {
                      Symptoms.add("Headache");
                    }

                    if(_isSelected7 == true)
                    {
                      Symptoms.add("Diarrhea");
                    }

                    if(_isSelected8 == true)
                    {
                      Symptoms.add("Nausea");
                    }

                    print(Symptoms);
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => Results()));
                  },
                )

            )


          ],
         ),
    );
  }
}

//Zsuzsanna
class Results extends StatelessWidget {
  var _symptomCounter = 8;
  var _symptomTotal = 1;
  var _comorbiditiesTotal = 0;
  var _age = 79;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffb2e8ff),
      appBar: AppBar(
        backgroundColor: new Color(0xffb2e8ff),
        centerTitle: true,
        title: Text(
          'Results',
          style: TextStyle(color: Colors.red),
        ),
        leading: IconButton(
          icon: Icon(Icons.repeat),
          onPressed: () {
            showAlertDialog(context);
          },
        ),
        // title: Text('Results'),
        //actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: 20,
                ),
                // child: Text('Results', style: TextStyle(fontSize: 30,color: Colors.deepOrange, ),),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(
                    color: new Color(0xffe0f5ff),
                    child: tableToUse(_symptomCounter)),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(
                  color: new Color(0xffe0f5ff),
                  child: cardToUse(_symptomTotal, _comorbiditiesTotal),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(
                  color: new Color(0xffe0f5ff),
                  child: ageCardToUse(_age),
                ),
              ),
              RaisedButton.icon(
                color: new Color(0xffe0f5ff),
                icon: Icon(Icons.repeat),
                hoverColor: Colors.deepOrange,
                label: Text(
                  'Retry',
                  style: TextStyle(fontSize: 20),
                ),
                onPressed: () {
                      showAlertDialog(context);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> showAlertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            //idea and some code from StackOverflow
            content: new Container(
              width: 260.0,
              height: 275.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
              ),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  // dialog top
                  new Expanded(
                    child: new Row(
                      children: <Widget>[
                        Center(
                          child: new Container(
                            // padding: new EdgeInsets.all(10.0),
                            decoration: new BoxDecoration(
                              color: Colors.white,
                            ),
                            child: Text(
                              'You Are About to Erase All Input!',
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 18.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // dialog centre
                  new Expanded(
                    child: new Container(
                        child: new Text('Are you sure you want to reset?',
                          style: TextStyle(fontSize: 20),

                        )),
                    flex: 1,
                  ),

                  // dialog bottom
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                        color: const Color(0xffe0f5ff),
                      ),
                      child: FlatButton(
                        child: Text('YES, RESET', style: TextStyle(color: Colors
                            .black, fontSize: 18),),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => UserInfo()));
                        },
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 10,
                  ),
                  // dialog bottom
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                        color: const Color(0xffe0f5ff),
                      ),
                      child: FlatButton(
                        child: Text('NO, BACK TO RESULTS', style: TextStyle(color: Colors
                            .black, fontSize: 18),),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),

                ],
              ),
            ),
          );
        });
  }

  Widget ageCardToUse(int age) {
    if (age < 60)
      return SafeAge();
    else
      return DangerAge();
  }

  Widget cardToUse(int symp, int comorbs) {
    if (symp == 1 && comorbs == 0)
      return YellowCardTwo();
    else if (symp == 0 && comorbs >= 1)
      return YellowCardOne();
    else if (symp >= 1 && comorbs >= 1)
      return RedCardOne();
    else if (symp >= 2 && comorbs == 0)
      return RedCardTwo();
    else
      return GreenCard();
  }

  Widget tableToUse(int count) {
    if (count <= 4)
      return DataTableUser4();
    else if (count <= 8)
      return DataTableUser8();
    else
      return DataTableUser12();
  }
}

class SafeAge extends StatelessWidget {
  SafeAge({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.green[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.cake,
              size: 40,
            ),
            title: Text(
              'Safe Age',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'Your age is in the population that is considered to be safer.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class DangerAge extends StatelessWidget {
  DangerAge({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.red[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.add_alert,
              size: 40,
            ),
            title: Text(
              'Cautious Age',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'Your age is in the section of the population that needs to practice safer protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class RedCardOne extends StatelessWidget {
  RedCardOne({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.red[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.bug_report,
              size: 40,
            ),
            title: Text(
              'Seek Medical Advice',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing at least one symptom and have at least one co-morbidity. Seek medical advice as soon as possible.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class RedCardTwo extends StatelessWidget {
  RedCardTwo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.red[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.bug_report,
              size: 40,
            ),
            title: Text(
              'Seek Medical Advice',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing symptoms and have at least one co-morbidity. Seek medical advice as soon as possible.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class GreenCard extends StatelessWidget {
  GreenCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.green[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.check,
              size: 40,
            ),
            title: Text(
              'No Signs of Infection',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing no symptoms. Keep practicing all safety protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class YellowCardOne extends StatelessWidget {
  YellowCardOne({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.yellow[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.help_outline,
              size: 40,
            ),
            title: Text(
              'Caution Advised',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing no symptoms, but have at least one co-mobidity. Keep practicing all safety protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class YellowCardTwo extends StatelessWidget {
  YellowCardTwo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.yellow[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.help_outline,
              size: 40,
            ),
            title: Text(
              'Caution Advised',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing a symptom. It is not possible to determine an infection. Keep practicing all safety protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class DataTableUser12 extends StatelessWidget {
  DataTableUser12({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columnSpacing: 15,
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Input',
            style: TextStyle(
              fontSize: 20,
              color: Colors.red,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            ' ',
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Gender',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('female', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Age',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('43', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Co-morbidities',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('weight')),
            DataCell(Text('diabetes')),
            DataCell(Text('resp. disease')),
            DataCell(Text('heart disease')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Symptoms',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
      ],
    );
  }
}

class DataTableUser8 extends StatelessWidget {
  DataTableUser8({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columnSpacing: 15,
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Input',
            style: TextStyle(
              fontSize: 20,
              color: Colors.red,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            ' ',
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Gender',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('female', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Age',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('43', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Co-morbidities',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('weight')),
            DataCell(Text('diabetes')),
            DataCell(Text('resp. disease')),
            DataCell(Text('heart disease')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Symptoms',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
      ],
    );
  }
}

class DataTableUser4 extends StatelessWidget {
  DataTableUser4({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columnSpacing: 15,
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Input',
            style: TextStyle(
              fontSize: 20,
              color: Colors.red,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            ' ',
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Gender',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('female', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Age',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text('43', style: TextStyle(fontSize: 15))),
            DataCell(Text('')),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Co-morbidities',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('weight')),
            DataCell(Text('diabetes')),
            DataCell(Text('resp. disease')),
            DataCell(Text('heart disease')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Symptoms',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))),
            DataCell(Text('fever')),
            DataCell(Text('cough')),
            DataCell(Text('anosmia')),
            DataCell(Text('chills')),
          ],
        ),
      ],
    );
  }
}
